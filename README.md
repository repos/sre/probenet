# Probenet

## Quick Demo

```javascript
// Function to be called after probe is completed
function onComplete(report) {
    console.log(report);
}

// Create Probenet object
let probe_net = new Probenet();

// Set probe recipe using a recipe url
probe_net.setRecipeUrl('<recipe-url>');

// Set probe recipe using a recipe object
// probe_net.setRecipeJson(recipe_object);

// Use FetchAPI to make requests (Default)
// probe_net.useFetch();

// Use XMLHttpRequest to make requests
// probe_net.useXHR();

// Set probe logger function (Default: No logs)
// probe_net.setLogger((log) => { console.log(log); });

// Run the probe
probe_net.runProbe(onComplete);
```
