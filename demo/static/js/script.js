function onComplete(report) {
    console.log(report);
}


function doProbe() {
    let probe_net = new Probenet();
    probe_net.setRecipeJson(RECIPE);
    probe_net.setLogger((log) => { console.log(log); });
    probe_net.runProbe(onComplete);
}


// https://developer.mozilla.org/en-US/docs/Web/API/Window/load_event
window.addEventListener("load", (event) => {
    doProbe();
});
